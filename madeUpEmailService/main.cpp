#include <bits/stdc++.h>
#include <limits>

class emailMessage {
    std::string recipient {""}, msg {""};
public:
    emailMessage(std::string r = "", std::string m = "") : recipient{r}, msg{m} {}
    std::string getRecipient() const;
    std::string getMessage() const;
};

std::string emailMessage::getRecipient() const {
    return this->recipient;
}

std::string emailMessage::getMessage() const {
    return this->msg;
}

class emailAccount {
    std::map<std::string, std::string> credentials;
    std::vector<emailMessage> sentMsgs;
public:
    void setCredentials(std::vector<emailAccount>);
    void sendEmailMessage(const std::vector<emailAccount> &accounts);
    void listAllSentEmailMessages() const;
    std::string getEmailAddress() const;
    std::string getPassword() const;
    std::vector<emailMessage> getSentEmails() const;
};

void emailAccount::setCredentials(std::vector<emailAccount> acc) {
    std::string e = "", confirmEmail = "", p = "", confirmPassword = "";
    std::cout << "Enter a new email address (\'quit\' to quit): ";
    std::getline(std::cin >> std::ws, e);
    if (e == "quit") {
        std::cout << "=============================================\nExiting to main menu.\n=============================================" << std::endl;
        throw 69;
    }
    while (e.find("@") == std::string::npos || e.find(".com") == std::string::npos) {
        std::cout << "=============================================\nError: Email should have an \"@\" symbol followed by the domain name.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Enter a new email address (\'quit\' to quit): ";
        std::getline(std::cin >> std::ws, e);
    }
    for (char i : e) {
        if (i == ' ') {
            std::cout << "=============================================\nError: Email should only have one word.\nExiting to main menu.\n=============================================" << std::endl;
            throw 69;
        }
    }
    // Check if email address already exists
    for (int i = 0; i < acc.size(); i++) {
        if (e == acc[i].getEmailAddress()) {
            std::cout << "=============================================\nError: Email address already exists.\nExiting to main menu.\n=============================================" << std::endl;
            throw 69;
        }
    }
    std::cout << "Confirm the new email address: ";
    std::getline(std::cin >> std::ws, confirmEmail);
    while (e.find("@") == std::string::npos || e.find(".com") == std::string::npos) {
        std::cout << "=============================================\nError: Email should have an \"@\" symbol followed by the domain name.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Confirm the new email address: ";
        std::getline(std::cin >> std::ws, confirmEmail);
    }
    for (char i : e) {
        if (i == ' ') {
            std::cout << "=============================================\nError: Email should only have one word.\nExiting to main menu.\n=============================================" << std::endl;
            throw 69;
        }
    }
    if (!(confirmEmail == e)) {
        std::cout << "=============================================\nError: Emails don't match.\nExiting to main menu.\n=============================================" << std::endl;
        throw 69;
    }
    std::cout << "Enter a new password: ";
    while (!(std::cin >> p)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "=============================================\nError: Invalid Input.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Enter a new password: ";
    }
    std::cout << "Confirm your password: ";
    while (!(std::cin >> confirmPassword)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "=============================================\nError: Invalid Input.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Confirm your password: ";
    }
    if (!(confirmPassword == p)) {
        std::cout << "=============================================\nError: Passwords don't match.\nExiting to main menu.\n=============================================" << std::endl;
        throw 69;
    }
    this->credentials.insert(std::pair<std::string,std::string>(e, p));
    auto it = credentials.begin();
    std::cout << "=============================================\nAccount \"" << it->first << "\" with password \"" << it->second << "\" created!\n=============================================" << std::endl;
}

void emailAccount::sendEmailMessage(const std::vector<emailAccount> &accounts) {
    std::string recipient, msg;

    std::cout << "Enter the recipient email address (\'quit\' to quit): ";
    std::getline(std::cin >> std::ws, recipient);
    if (recipient == "quit") {
        std::cout << "=============================================\nExiting to main menu.\n=============================================" << std::endl;
        return;
    }
    while (recipient.find('@') == std::string::npos || recipient.find(".com") == std::string::npos) {
        std::cout << "=============================================\nError: Email should have an \"@\" symbol followed by the domain name.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Enter the recipient email address (\'quit\' to quit): ";
        std::getline(std::cin >> std::ws, recipient);
    }
    for (char i : recipient) {
        if (i == ' ') {
            std::cout << "=============================================\nError: Email should only have one word.\nExiting to main menu.\n=============================================" << std::endl;
            return;
        }
    }

    // Check if recipient email account exists in the "server" (aka the vector)
    bool ifExists = false;
    for (auto i : accounts) {
        if (i.getEmailAddress() == recipient) {
            ifExists = true;
        }
    }
    if (ifExists == false) {
        std::cout << "=============================================\nError: Recipient account does not exist in the system. Unable to send email.\nExiting to main menu.\n=============================================" << std::endl;
        return;
    }

    std::cout << "Enter the message: ";
    std::getline(std::cin >> std::ws, msg);

    emailMessage temp (recipient, msg);
    this->sentMsgs.push_back(temp);
    std::cout << "The email message was successfully sent!" << std::endl;
}

void emailAccount::listAllSentEmailMessages() const {
    // Check if no email messages found
    if (this->sentMsgs.empty()) {
        std::cout << "=============================================\nError: No emails found.\n=============================================" << std::endl;
        return;
    }
    // List each email message + recipient
    for (int i = 0; i < this->sentMsgs.size(); i++) {
        std::cout << "=============================================\nEmail Message: #" << (i+1) << "\nRecipient: " << this->sentMsgs[i].getRecipient() << "\nMessage: " << this->sentMsgs[i].getMessage() << "\n=============================================" << std::endl;
    }
}

std::string emailAccount::getEmailAddress() const {
    // Retrieve and Return Email Address From Credentials Map
    auto it = this->credentials.begin();
    return it->first;
}

std::string emailAccount::getPassword() const {
    // Retrieve and Return Password From Credentials Map
    auto it = this->credentials.begin();
    return it->second;
}

std::vector<emailMessage> emailAccount::getSentEmails() const {
    return this->sentMsgs;
}

void mainMenu();
void createAccount(std::vector<emailAccount> &accounts);
void loginMenu(std::vector<emailAccount> &);
bool ifMatchesUser(std::string e, std::string p, const std::vector<emailAccount> &);
void accountMenu(emailAccount &acc, const std::vector<emailAccount> &accounts);
void accountMenuOptions();
void displayIncomingMail(const std::vector<emailAccount>&, const emailAccount);
void deleteEmailAccount(std::vector<emailAccount> &);

int main() {

    std::vector<emailAccount> accounts;

    bool quit = false;

    while (quit == false) {
        mainMenu();
        int menuOp;
        while (!(std::cin >> menuOp)) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "=============================================\nError: Invalid Input.\nPlease try again.\n=============================================" << std::endl;
            mainMenu();
        }

        while (menuOp < 1 || menuOp > 4) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "=============================================\nError: Invalid Input.\nPlease try again.\n=============================================" << std::endl;
            mainMenu();
        }

        switch (menuOp) {
            case 1: {
                createAccount(accounts);
                break;
            }
            case 2: {
                loginMenu(accounts);
                break;
            }
            case 3: {
                deleteEmailAccount(accounts);
                break;
            }
            case 4: {
                quit = true;
                std::cout << "=============================================\nProgram Exiting.\n=============================================" << std::endl;
                break;
            }
        }
    }

    return 0;
}

void mainMenu() {
    std::cout << "Main Menu:\n"
                 "[1] Create Account\n"
                 "[2] Login\n"
                 "[3] Delete Account\n"
                 "[4] Exit\n"
                 "Option: ";
}

void createAccount(std::vector<emailAccount> &accounts) {
    emailAccount temp;
    try {
        temp.setCredentials(accounts);
    }
    catch (int a) {
        return;
    }
    accounts.push_back(temp);
}

void loginMenu(std::vector<emailAccount> &accounts) {
    // Error Check (If no accounts are in the vector)
    if (accounts.empty()) {
        std::cout << "=============================================\nError: No accounts found.\n=============================================" << std::endl;
        return;
    }

    std::string e, p;
    std::cout << "Please enter your email address (\'quit\' to quit): ";
    std::getline(std::cin >> std::ws, e);
    if (e == "quit") {
        std::cout << "=============================================\nExiting to main menu.\n=============================================" << std::endl;
        return;
    }
    while (e.find("@") == std::string::npos || e.find(".com") == std::string::npos) {
        std::cout << "=============================================\nError: Email should have an \"@\" symbol followed by the domain name.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Please enter your email address (\'quit\' to quit): ";
        std::getline(std::cin >> std::ws, e);
    }
    for (char i : e) {
        if (i == ' ') {
            std::cout << "=============================================\nError: Email should only have one word.\nExiting to main menu.\n=============================================" << std::endl;
            return;
        }
    }
    std::cout << "Please enter your password: ";
    while (!(std::cin >> p)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "=============================================\nError: Invalid Input.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Please enter your password: ";
    }

    if (ifMatchesUser(e, p, accounts)) {
        std::cout << "=============================================\nSuccessfully logged in!\n=============================================" << std::endl;
        // Find index of account found
        int index = 0;
        for (int i = 0; i < accounts.size(); i++) {
            if (e == accounts[i].getEmailAddress() && p == accounts[i].getPassword()) {
                index = i;
            }
        }
        accountMenu(accounts[index], accounts);
    } else {
        std::cout << "=============================================\nError: No user matching provided credentials found.\n=============================================" << std::endl;
    }
}

bool ifMatchesUser(std::string e, std::string p, const std::vector<emailAccount> &accounts) {
    bool ret = false;
    for (auto i : accounts) {
        if (e == i.getEmailAddress() && p == i.getPassword()) {
            ret = true;
        }
    }
    return ret;
}

void accountMenu(emailAccount &acc, const std::vector<emailAccount>& accounts) {
    bool quit = false;
    while (quit == false) {
        accountMenuOptions();
        int accMenuOp;
        std::cin >> accMenuOp;
        while (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "=============================================\nError: Invalid Input.\nPlease try again.\n=============================================" << std::endl;
            accountMenuOptions();
            std::cin >> accMenuOp;
        }

        while (accMenuOp < 1 || accMenuOp > 4) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "=============================================\nError: Invalid Input.\nPlease try again.\n=============================================" << std::endl;
            accountMenuOptions();
        }
        switch (accMenuOp) {
            case 1: {
                acc.sendEmailMessage(accounts);
                break;
            }
            case 2: {
                displayIncomingMail(accounts, acc);
                break;
            }
            case 3: {
                acc.listAllSentEmailMessages();
                break;
            }
            case 4: {
                quit = true;
                std::cout << "=============================================\nLogging out.\n=============================================" << std::endl;
                break;
            }
        }
    }
}

void accountMenuOptions() {
    std::cout << "Account Menu:\n"
                 "[1] Send an email message\n"
                 "[2] List all incoming messages\n"
                 "[3] List all sent email messages\n"
                 "[4] Log out\n"
                 "Option: ";
}

void displayIncomingMail(const std::vector<emailAccount> &accs, const emailAccount recipient) {
    int incomingEmailCount = 0;

    for (int i = 0; i < accs.size(); i++) {
        for (int j = 0; j < accs[i].getSentEmails().size(); j++) {
            if (recipient.getEmailAddress() == accs[i].getSentEmails()[j].getRecipient()) {
                incomingEmailCount++;
                std::cout << "=============================================\nEmail Message: #" << (incomingEmailCount) << "\nSender: " << accs[i].getEmailAddress() << "\nMessage: " << accs[i].getSentEmails()[j].getMessage() << "\n=============================================" << std::endl;
            }
        }
    }

    if (incomingEmailCount == 0) {
        std::cout << "=============================================\nError: No emails found.\n=============================================" << std::endl;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        return;
    }
}

void deleteEmailAccount(std::vector<emailAccount> &accounts) {
    if (accounts.empty()) {
        std::cout << "=============================================\nError: No accounts found.\n=============================================" << std::endl;
        return;
    }

    std::string e, p;
    std::cout << "Please enter your email address (\'quit\' to quit): ";
    std::getline(std::cin >> std::ws, e);
    if (e == "quit") {
        std::cout << "=============================================\nExiting to main menu.\n=============================================" << std::endl;
        return;
    }
    while (e.find("@") == std::string::npos || e.find(".com") == std::string::npos) {
        std::cout << "=============================================\nError: Email should have an \"@\" symbol followed by the domain name.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Please enter your email address (\'quit\' to quit): ";
        std::getline(std::cin >> std::ws, e);
    }
    for (char i : e) {
        if (i == ' ') {
            std::cout << "=============================================\nError: Email should only have one word.\nExiting to main menu.\n=============================================" << std::endl;
            return;
        }
    }

    bool ifFound = false;
    std::vector<emailAccount>::iterator toDelete;
    for (auto it = accounts.begin(); it != accounts.end(); it++) {
        if (it->getEmailAddress() == e) {
            ifFound = true;
            toDelete = it;
        }
    }

    if (ifFound == false) {
        std::cout << "=============================================\nError: No accounts found with that email address.\n=============================================" << std::endl;
        return;
    }

    std::cout << "Please enter your password: ";
    while (!(std::cin >> p)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "=============================================\nError: Invalid Input.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Please enter your password: ";
    }

    std::cout << "Are you sure you want to delete this account? (\'yes\' for yes): ";
    std::string ans;
    std::cin >> ans;
    while (std::cin.fail()) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "=============================================\nError: Invalid Input.\nPlease try again.\n=============================================" << std::endl;
        std::cout << "Are you sure you want to delete this account? (\'yes\' for yes): ";
        std::cin >> ans;
    }

    if (ans == "yes") {
        std::cout << "=============================================\nDeleting account.\n=============================================" << std::endl;
        accounts.erase(toDelete);
    } else {
        std::cout << "=============================================\nExiting to main menu.\n=============================================" << std::endl;
    }
}
