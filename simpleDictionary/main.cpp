#include <iostream>
#include <bits/stdc++.h>

void mainMenu();
void addEntry(std::map<std::string,std::string> &dict);
void deleteEntry(std::map<std::string,std::string> &dict);
void editTranslation(std::map<std::string,std::string> &dict);
void searchEntry(const std::map<std::string,std::string> &dict);
void saveEntries(const std::map<std::string,std::string> &dict);
void readEntries(std::map<std::string,std::string> &dict);

int main() {
    // Create Dictionary
    std::map<std::string,std::string> dictionary;
    // Read Entries From File
    readEntries(dictionary);
    // Main Menu
    bool quit = false;
    while (quit == false) {
        mainMenu();
        int menuOp;
        std::cin >> menuOp;
        // Error-Checking (Non-Int)
        if (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(256, '\n');
            continue;
        }
        // Error-Checking (Out of Range of Menu Items)
        if (menuOp < 1 || menuOp > 5) {
            continue;
        }
        std::cout << "=====================================" << std::endl;
        // Switch Menu
        switch (menuOp) {
            // Add Entry
            case 1: {
                addEntry(dictionary);
                break;
            }
            // Delete Entry
            case 2: {
                deleteEntry(dictionary);
                break;
            }
            // Edit Entry
            case 3: {
                editTranslation(dictionary);
                break;
            }
            // Search Entry
            case 4: {
                searchEntry(dictionary);
                break;
            }
            // Exit
            case 5: {
                quit = true;
                break;
            }
        }
    }
    // Ask If User Wants To Save Everything To A File
    bool quitE = false;
    while (quitE == false) {
        std::cout << "Do you want to save your changes? (Y/N): ";
        char ans;
        std::cin >> ans;
        std::cout << "=====================================" << std::endl;
        // Error-Checking (Non-Int)
        if (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(256, '\n');
            continue;
        }
        // Switch Menu
        switch(ans) {
            case 'Y': {
                saveEntries(dictionary);
                quitE = true;
                break;
            }
            case 'N': {
                quitE = true;
                break;
            }
            case 'y': {
                saveEntries(dictionary);
                quitE = true;
                break;
            }
            case 'n': {
                quitE = true;
                break;
            }
            default: {
                std::cout << "ERROR: Unknown Option." << std::endl;
                break;
            }
        }
    }
    // Exit
    return 0;
}

void mainMenu() {
    std::cout << "=====================================\n"
                 "Main Menu: \n"
                 "=====================================\n"
                 "[1] Add Entry\n"
                 "[2] Delete Entry\n"
                 "[3] Edit Entry (Only Translation)\n"
                 "[4] Search Entry\n"
                 "[5] Quit\n"
                 "=====================================\n"
                 "Option: ";
}

void addEntry(std::map<std::string,std::string> &dict) {
    // Ask for Word + Translation
    std::string tempW, tempT;
    std::cout << "Enter Chavacano Word: ";
    std::getline(std::cin >> std::ws, tempW);
    std::cout << "Enter English Translation: ";
    std::getline(std::cin >> std::ws, tempT);
    // Check if Entry Already Exists
    for (const auto& i : dict) {
        // Exit If Exists
        if (i.first == tempW) {
            return;
        }
    }
    // Add Entry
    dict.insert(std::pair<std::string, std::string>(tempW, tempT));
}

void deleteEntry(std::map<std::string,std::string> &dict) {
    // Error-Checking (If Map Is Empty)
    if (dict.empty()) {
        std::cout << "ERROR: No Entries Found." << std::endl;
        return;
    }
    // Show All Entries
    std::cout << "List of Entries: " << std::endl;
    for (const auto& i : dict) {
        std::cout << i.first << "\t" << i.second << std::endl;
    }
    // Select Key
    std::cout << "(Select Word): ";
    std::string tempW;
    std::getline(std::cin >> std::ws, tempW);
    // Check If Key Exists
    bool ifExists = false;
    for (const auto& i : dict) {
        if (tempW == i.first) {
            ifExists = true;
        }
    }
    // Exit If Key Doesn't Exist
    if (!ifExists) {
        std::cout << "ERROR: Entry Not Found Using Inputted Word." << std::endl;
        return;
    }
    // Delete Entry At Specific Key
    dict.erase(tempW);
}

void editTranslation(std::map<std::string,std::string> &dict) {
    // Error-Checking (If Map Is Empty)
    if (dict.empty()) {
        std::cout << "ERROR: No Entries Found." << std::endl;
        return;
    }
    // Edit Translation
    std::cout << "List of Entries: " << std::endl;
    for (const auto& i : dict) {
        std::cout << i.first << "\t" << i.second << std::endl;
    }
    std::string tempW;
    std::cout << "(Select Which Word Whose Translation You Want To Change): ";
    std::getline(std::cin >> std::ws, tempW);
    // Check If Key Exists
    bool ifExists = false;
    for (const auto& i : dict) {
        if (tempW == i.first) {
            ifExists = true;
        }
    }
    // Exit If Key Doesn't Exist
    if (!ifExists) {
        std::cout << "ERROR: Entry Not Found Using Inputted Word." << std::endl;
        return;
    }
    // Edit Translation Menu
    bool quit = false;
    while (!quit) {
        std::cout << "Word: " << dict.find(tempW)->first << std::endl
                  << "Translation: " << dict.find(tempW)->second << std::endl
                  << "Options: \n"
                     "[1] Edit Translation\n"
                     "[2] Exit to Main Menu\n"
                     "Option: ";
        int menuOp;
        std::cin >> menuOp;
        // Error-Checking (Non-Int)
        if (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(256, '\n');
            continue;
        }
        // Error-Checking (Out of Range of Menu Items)
        if (menuOp < 1 || menuOp > 2) {
            continue;
        }
        // Switch Menu
        switch (menuOp) {
            case 1: {
                // Change Translation
                std::cout << "(Enter New Translation): ";
                std::string tempT;
                std::getline(std::cin >> std::ws, tempT);
                dict.find(tempW)->second = tempT;
                break;
            }
            case 2: {
                quit = true;
                break;
            }
        }
    }
}

void searchEntry(const std::map<std::string,std::string> &dict) {
    // Error-Checking (If Map Is Empty)
    if (dict.empty()) {
        std::cout << "ERROR: No Entries Found." << std::endl;
        return;
    }
    // Search For Specific Entry
    std::string tempW;
    std::cout << "(Enter Word to Find Entry): ";
    std::getline(std::cin >> std::ws, tempW);
    // Search Map
    bool ifFound = false;
    for (const auto& i : dict) {
        if (tempW == i.first) {
            ifFound = true;
        }
    }
    // Exit If Not Found
    if (!ifFound) {
        std::cout << "ERROR: Entry Not Found Using Inputted Word." << std::endl;
        return;
    }
    // Display Entry
    std::cout << "Word: " << dict.find(tempW)->first << std::endl
              << "Translation: " << dict.find(tempW)->second << std::endl;
}

void saveEntries(const std::map<std::string,std::string> &dict) {
    // Save Entries to File
    std::ofstream writeFile("dictionary.txt");
    for (const auto& i : dict) {
        writeFile << i.first << " " << i.second << std::endl;
    }
    // Close File
    writeFile.close();
    // Display Amount of Entries Read From File
    std::cout << "=====================================" << std::endl;
    std::cout << "(" << dict.size() << " entries saved to file.)" << std::endl;
    std::cout << "=====================================" << std::endl;
}

void readEntries(std::map<std::string,std::string> &dict) {
    // Read Entries From File
    std::ifstream readFile("dictionary.txt");
    std::string tempLine, tempW, tempT;
    // Read Each Line From File
    while (std::getline(readFile, tempLine, '\n')) {
        std::stringstream ss (tempLine);
        ss >> tempW;
        std::getline(ss, tempT);
        // Insert Entry Into Dictionary
        dict.insert(std::pair<std::string,std::string>(tempW, tempT));
    }
    // Close File
    readFile.close();
    // Display Amount of Entries Read From File
    std::cout << "=====================================" << std::endl;
    std::cout << "(" << dict.size() << " entries read from file.)" << std::endl;
}