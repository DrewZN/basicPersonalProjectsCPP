# basicPersonalProjectsCPP
Just some basic personal projects written in C++

|Project|Description|
|-------|-----------|
|**bankingSystem**|Menu-driven banking / ATM system that allows for account creation/deletion, depositing/withdrawing, displaying balance, and saving/writing from database files.|
|**basicFlashcardSystem**|Menu-driven flashcard system. Allows for creation, editing, deletion, and limited practice of flashcards.|
|**basicHangman**|Hangman. Displays correct letters. Needs to be fixed so that each limb is accounted for as well. Not just each 'row'.|
|**basicLoginSystem**|Menu-driven login system that allows for the creation and deletion of users. 'Saving to/Reading from file' system added (not 100% sure if it works yet).|
|**basicTicTacToe**|Tic-Tac-Toe. Two players.|
|**basicVendingMachine**|WIP - Command-driven vending machine system.|
|**birthdayReminder**|Menu-driven birthday reminder system. Incomplete.|
|**contactSystem**|Menu-driven contact management system that allows for the creation and deletion of phone contacts. Allows for searching for as well as editing individual contacts. Needs some bug-fixing (check todo).|
|**madeUpEmailService**|Mostly complete "emailing client". (aka emails are all managed on runtime.) Needs some more features |
|**simpleDictionary**|Menu-driven, file-saving/reading system for adding, editing, viewing, and deleting dictionary entries using std::map. (WIP)|
|**studentRecordSystem**|Menu-driven student record system. Allows for creation, deletion, and modification of individual student records.|
